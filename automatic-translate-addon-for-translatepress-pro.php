<?php
/**
 * Plugin Name: Automatic Translate Addon For TranslatePress (Pro)
 * Description: Auto language translator add-on for TranslatePress official plugin to translate website into any language via fully automatic machine translations via Yandex Translate Widget.
 * Author: Cool Plugins
 * Author URI: https://coolplugins.net/
 * Plugin URI:
 * Version: 1.2.2
 * License: GPL2
 * Text Domain:TPAP
 * Domain Path: languages
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( defined( 'TPAP_VERSION' ) ) {
	return;
}
define( 'TPAP_VERSION', '1.2.2' );
define( 'TPAP_FILE', __FILE__ );
define( 'TPAP_PATH', plugin_dir_path( TPAP_FILE ) );
define( 'TPAP_URL', plugin_dir_url( TPAP_FILE ) );
if ( ! class_exists( 'TranslatePress_Addon_Pro' ) ) {
	final class TranslatePress_Addon_Pro {

		/**
		 * Plugin instance.
		 *
		 * @var TranslatePress_Addon_Pro
		 * @access private
		 */
		private static $instance = null;

		/**
		 *  Construct the plugin object
		 */
		private function __construct() {
			register_activation_hook( __FILE__, array( $this, 'tpap_activate' ) );
			add_filter( 'trp_string_groups', array( $this, 'tpap_string_groups' ) );
			add_action( 'plugins_loaded', array( $this, 'tpap_check_required_plugin' ) );
			if ( ! is_admin() ) {
				add_action( 'trp_translation_manager_footer', array( $this, 'tpap_register_assets' ) );
			}
			add_action( 'wp_ajax_tpap_get_strings', 'tpap_getstrings' );
			add_action( 'wp_ajax_tpap_save_translations', 'tpap_save_translations' );
			add_filter( 'trp_translation_manager_footer', array( $this, 'add_custom_class' ) );

			add_action( 'init', array( $this, 'set_gtranslate_cookie' ) );
			if ( is_admin() ) {
				add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'tpap_settings_page_link' ) );
				add_action( 'admin_init', array( $this, 'tpap_is_free_version_active' ) );
				add_action( 'init', array( $this, 'tpap_onInit' ) );
			}
			add_action( 'init', array( $this, 'tpap_set_gtranslate_cookie' ) );
			$this->tpap_required_files();

		}
			  /**
			   * create 'settings' link in plugins page
			   */
		public function tpap_settings_page_link( $links ) {
			$links[] = '<a style="font-weight:bold" href="' . esc_url( get_admin_url( null, 'options-general.php?page=translatepress-tpap-register' ) ) . '">License</a>';
			return $links;
		}
		public function tpap_onInit() {
			$key = get_option( 'TranslatepressAutomaticTranslateAddonPro_lic_Key' );
			if ( empty( $key ) ) {
				add_action( 'admin_notices', array( $this, 'tpap_add_license_notice' ) );
			}
		}
		public function tpap_add_license_notice() {
			 $settings_page_link = esc_url( get_admin_url( null, 'options-general.php?page=translatepress-tpap-register' ) );
			$notice              = __( '<strong> Automatic Translate Addon For TranslatePress (Pro)</strong> - License key is missing! Please add your License key in the settings panel to activate all premium features.', 'tpap' );
			echo '<div class="error auto-translatepress-pro-missing" style="border:2px solid;border-color:#dc3232;"><p>' . $notice . '</p>
                <p><a class="button button-primary" href="' . $settings_page_link . '">' . __( 'Add License Key' ) . '</a> (You can find license key inside order purchase email or visit <a href="https://coolplugins.net/my-account/orders/)" target="_blank">https://coolplugins.net/my-account/orders/</a>)</p></div>';
		}
		/**
		 * Get plugin instance.
		 *
		 * @return TranslatePress_Addon_Pro
		 * @static
		 */
		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}
		public function tpap_is_free_version_active() {
			if ( is_plugin_active( 'automatic-translate-addon-for-translatepress/automatic-translate-addon-for-translatepress.php' ) ) {
				deactivate_plugins( 'automatic-translate-addon-for-translatepress/automatic-translate-addon-for-translatepress.php' );

				add_action(
					'admin_notices',
					function () {?>
			<style>div#message.updated {
				display: none;
			}</style>
				<div class="notice notice-error is-dismissible">
					<p>
						  <?php
							_e( 'Automatic Translate Addon For TranslatePress : Automatic Translate Addon For TranslatePress is <strong>deactivated</strong> as you have already activated the pro version.', 'tpap' );
							?>
					</p>
				</div>

						  <?php
					}
				);
			}
		}
		public function tpap_required_files() {
			 require_once TPAP_PATH . 'includes/tpap-functions.php';

		}

		public function set_gtranslate_cookie() {
			// setting your cookies there
			if ( ! isset( $_COOKIE['googtrans'] ) ) {
				setcookie( 'googtrans', '/en/Select Language', 2147483647 );
			}
		}
		public function add_custom_class( $classes ) {
			return "$classes notranslate";
		}

		// set settings on plugin activation
		public function tpap_activate() {
			update_option( 'tpap-v', TPAP_VERSION );
			update_option( 'tpap-type', 'PRO' );
			update_option( 'tpap-pro-installDate', date( 'Y-m-d h:i:s' ) );
			update_option( 'tpap-pro-ratingDiv', 'no' );
		}
		/**
		 * Change string groups
		 */
		public function tpap_string_groups() {
			$string_groups = array(
				'slugs'           => 'Slugs',
				'metainformation' => 'Meta Information',
				'stringlist'      => 'String List',
				'gettextstrings'  => 'Gettext Strings',
				'images'          => 'Images',
				'dynamicstrings'  => 'Dynamically Added Strings',
			);
			return $string_groups;
		}
		/*
		|----------------------------------------------------------------------
		| check if required "TranslatePress - Multilingual" plugin is active
		| also register the plugin text domain
		|----------------------------------------------------------------------
		 */
		public function tpap_check_required_plugin() {
			if ( ! function_exists( 'trp_enable_translatepress' ) ) {
				add_action( 'admin_notices', array( $this, 'tpap_plugin_required_admin_notice' ) );
			}

			if ( is_admin() ) {
				require_once TPAP_PATH . 'admin/Registration/AutomaticTranslateAddonForTranslatePressBase.php';
				new AutomaticTranslateAddonForTranslatePressBase( TPAP_FILE );
				include_once TPAP_PATH . 'admin/Registration/TranslatepressAutomaticTranslateAddonPro.php';
				/** Feedback form after deactivation */
				/*** Plugin review notice file */
				require_once TPAP_PATH . 'admin/tpap-feedback-notice.php';
				new TPAPFeedbackNotice();
			}
			load_plugin_textdomain( 'TPA', false, basename( dirname( TPAP_FILE ) ) . '/languages/' );
		}
		/*
		|----------------------------------------------------------------------
		| Notice to 'Admin' if "TranslatePress - Multilingual" is not active
		|----------------------------------------------------------------------
		 */
		public function tpap_plugin_required_admin_notice() {
			if ( current_user_can( 'activate_plugins' ) ) {
				$url         = 'plugin-install.php?tab=plugin-information&plugin=TranslatePress - Multilingual&TB_iframe=true';
				$title       = 'TranslatePress - Multilingual';
				$plugin_info = get_plugin_data( TPAP_FILE, true, true );
				echo '<div class="error"><p>' .
				sprintf(
					__(
						'In order to use <strong>%1$s</strong> plugin, please install and activate the latest version  of <a href="%2$s" class="thickbox" title="%3$s">%4$s</a>',
						'automatic-translator-addon-for-loco-translate'
					),
					$plugin_info['Name'],
					esc_url( $url ),
					esc_attr( $title ),
					esc_attr( $title )
				) . '.</p></div>';
			}
		}
		/**
		 *  Register Assets
		 * Hooked to trp_translation_manager_footer.
		 */
		public function tpap_register_assets() {
			wp_register_script( 'tpap-custom-script', TPAP_URL . 'assets/js/tpap-custom-script.js', array( 'jquery', 'jquery-ui-dialog' ), TPAP_VERSION );
			wp_register_script( 'tpap-yandex-widget', TPAP_URL . 'assets/js/yandex-widget.js?widgetId=ytWidget&pageLang=en&widgetTheme=light&autoMode=false', array(), TPAP_VERSION, true );
			// wp_register_script( 'tpap-google', TPAP_URL . 'assets/js/google-widget.js', array(), TPAP_VERSION, true );
			wp_register_style( 'tpap-editor-styles', TPAP_URL . 'assets/css/tpap-custom.css', null, TPAP_VERSION, 'all' );
			$key                      = ! empty( get_option( 'TranslatepressAutomaticTranslateAddonPro_lic_Key' ) ) ? get_option( 'TranslatepressAutomaticTranslateAddonPro_lic_Key' ) : 'No';
			$extraData['gt_preview']  = TPAP_URL . '/assets/images/powered-by-google.png';
			$extraData['yt_preview']  = TPAP_URL . '/assets/images/powered-by-yandex.png';
			$extraData['ajax_url']    = admin_url( 'admin-ajax.php' );
			$extraData['nonce']       = wp_create_nonce( 'auto-translate-press-pro-nonces' );
			$extraData['plugin_url']  = plugins_url();
			$extraData['license_key'] = $key;
			$this->tpap_load_gtranslate_scripts();
			// wp_enqueue_script( 'tpap-google' );
			wp_enqueue_script( 'tpap-custom-script' );
			wp_localize_script( 'tpap-custom-script', 'extradata', $extraData );
			wp_enqueue_script( 'tpap-yandex-widget' );
			wp_print_styles( 'tpap-editor-styles' );
		}

		/*
		|----------------------------------------------------------------------
		| Google Translate Widget integrations
		| load Google Translate widget scripts
		|----------------------------------------------------------------------
		*/
		public function tpap_load_gtranslate_scripts() {

			echo "<script>
           function googleTranslateElementInit() {
               var locale=locoConf.conf.locale;
               var defaultcode = locale.lang?locale.lang:null;
               switch(defaultcode){
                   case 'bel':
                   defaultlang='be';
                   break;
                   case 'he':
                       defaultlang='iw';
                       break;
                   case'snd':
                       defaultlang='sd';
                   break;
                   case 'jv':
                       defaultlang='jw';
                       break;
                       case 'nb':
                           defaultlang='no';
                           break;
             
                           case 'nn':
                             defaultlang='no';
                             break;
                   default:
                   defaultlang=defaultcode;
               break;
               return defaultlang;
               }
              if(defaultlang=='zh'){
              new google.translate.TranslateElement(
                   {
                   pageLanguage: 'en',
                   includedLanguages: 'zh-CN,zh-TW',
                   defaultLanguage: 'zh-CN,zh-TW',
                   multilanguagePage: true
                   },
                   'google_translate_element'
               );
           }
           else{
               new google.translate.TranslateElement(
                   {
                   pageLanguage: 'en',
                   includedLanguages: defaultlang,
                   defaultLanguage: defaultlang,
                   multilanguagePage: true
                   },
                   'google_translate_element'
               );
           }
           }
           </script>
           <script src='https://translate.google.com/translate_a/element.js'></script>
           ";
		}

		// set default option in google translate widget using cookie
		public function tpap_set_gtranslate_cookie() {
			// setting your cookies there
			if ( ! isset( $_COOKIE['googtrans'] ) ) {
				setcookie( 'googtrans', '/en/Select Language', 2147483647 );
			}
		}
		/**
		 * TranslatePress_Addon_Pro Class Close
		 */
	}
}

$translatepress_pro_addon = TranslatePress_Addon_Pro::get_instance();
