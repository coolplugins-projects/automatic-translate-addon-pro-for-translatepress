<?php
namespace TranslatePressAutoTranslateAddon\Register;

	class TranslatepressAutomaticTranslateAddonPro {
        public $plugin_file=__FILE__;
        public $responseObj;
        public $licenseMessage;
        public $showMessage=false;
        public $slug="translatepress-tpap-register";
        function __construct() {
    	    add_action( 'admin_enqueue_scripts', [ $this, 'SetAdminStyle' ] );
    	    $licenseKey=get_option("TranslatepressAutomaticTranslateAddonPro_lic_Key","");
    	    $liceEmail=get_option( "TranslatepressAutomaticTranslateAddonPro_lic_email","");
            \AutomaticTranslateAddonForTranslatePressBase::addOnDelete(function(){
               delete_option("TranslatepressAutomaticTranslateAddonPro_lic_Key");
            });
            if(\AutomaticTranslateAddonForTranslatePressBase::CheckWPPlugin($licenseKey,$liceEmail,$this->licenseMessage,$this->responseObj,__FILE__)){
                add_action( 'admin_menu', [$this,'ActiveAdminMenu'],101);
                add_action( 'admin_post_TranslatepressAutomaticTranslateAddonPro_el_deactivate_license', [ $this, 'action_deactivate_license' ] );
                update_option("tpap-type","pro");
            }else{
                add_action('admin_menu',array($this,'tpap_register_my_custom_submenu_page'),11);
    	        if(!empty($licenseKey) && !empty($this->licenseMessage)){
    	            $this->showMessage=true;
                }
                add_action( 'admin_post_TranslatepressAutomaticTranslateAddonPro_tpap_activate_license', [ $this, 'action_activate_license' ] );
    		}
        }
        function action_deactivate_license() {
    	    check_admin_referer( 'tpap-license' );
    	    if(\AutomaticTranslateAddonForTranslatePressBase::RemoveLicenseKey(__FILE__,$message)){
                update_option("TranslatepressAutomaticTranslateAddonPro_lic_Key","") || add_option("TranslatepressAutomaticTranslateAddonPro_lic_Key","");
    	    }
    	   wp_safe_redirect(admin_url( 'admin.php?page='.$this->slug));
        }
        function ActiveAdminMenu(){
            add_options_page(
                __( 'TranslatePress - Auto Translate Addon', 'tpap' ),
                __( 'TranslatePress - Auto Translate Addon', 'tpap' ),
                'manage_options',
                $this->slug,
                array(
                    $this,
                    'Activated'
                ));
        }
    /**
     * Registers a new settings page under Settings.
     */
    function tpap_register_my_custom_submenu_page() {
        add_options_page(
            __( 'TranslatePress - Auto Translate Addon', 'tpap' ),
            __( 'TranslatePress - Auto Translate Addon', 'tpap' ),
            'manage_options',
            $this->slug,
            array(
                $this,
                'TpapLicenseForm'
            )
        );
    }
    function TpapLicenseForm() {
        ?>
    <form method="post" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>">
        <input type="hidden" name="action" value="TranslatepressAutomaticTranslateAddonPro_tpap_activate_license"/>
        <div class="tpap-license-container">
            <h3 class="tpap-license-title"><i class="dashicons-before dashicons-translation"></i> <?php _e("Automatic Translate Addon For TranslatePress - Premium License",$this->slug);?></h3>
            <div class="tpap-license-content">
                <div class="tpap-license-form">
                    <h3>Activate Premium License</h3>
                    <?php
                    if(!empty($this->showMessage) && !empty($this->licenseMessage)){
                        ?>
                        <div class="notice notice-error is-dismissible">
                            <p><?php echo _e($this->licenseMessage,$this->slug); ?></p>
                        </div>
                        <?php
                    }
                    ?>
                    <!--Enter License Key Here START-->
                    <div class="tpap-license-field">
                        <label for="tpap_license_key"><?php _e("Enter License code",$this->slug);?></label>
                        <input type="text" class="regular-text code" name="tpap_license_key" size="50" placeholder="xxxxxxxx-xxxxxxxx-xxxxxxxx-xxxxxxxx" required="required">
                    </div>
                    <div class="tpap-license-field">
                        <label for="tpap_license_key"><?php _e("Email Address",$this->slug);?></label>
                        <?php
                            $purchaseEmail   = get_option( "TranslatepressAutomaticTranslateAddonPro_lic_email", get_bloginfo( 'admin_email' ));
                        ?>
                        <input type="text" class="regular-text code" name="tpap_license_email" size="50" value="<?php echo sanitize_email($purchaseEmail); ?>" placeholder="" required="required">
                        <div><small><?php _e("✅ I agree to share my purchase code and email for plugin verification and to receive future updates notifications!",$this->slug);?></small></div>
                    </div>
                    <div class="tpap-license-active-btn">
                        <?php wp_nonce_field( 'tpap-license' ); ?>
                        <?php submit_button('Activate'); ?>
                    </div>
                    <!--Enter License Key Here END-->
                    
                    <br/>
                    <!-- <a class="button button-primary" href='https://coolplugins.net/product/automatic-translate-addon-for-translatepress-pro/' target='_blank'>Buy Pro Plugin + License</a> -->
                </div>
                <div class="tpap-license-textbox">
                        <h3>Important Points</h3>
                        <ol>
                        <li>1) Automatic translate providers do not support HTML and special characters translations. So plugin will not translate any string that contains HTML or special characters.</li>
                    <li>2) If any auto-translation provider stops any of its free translation service then plugin will not support that translation service provider.</li>
                          
                            <li>3) If you have any issue or query, please <a href="https://coolplugins.net/support/" target="_blank">contact support</a>.</li>
                            <li>4) Automatic translate plugins and themes strings via <a href="https://locoaddon.com/addon/loco-automatic-translate-premium-license-key/">Automatic Translate Addon For Loco Translate</a></li>
                        </ol>
                        <div class="tpap-pluginby">
                            Plugin by<br/>
                            <a href="https://coolplugins.net" target="_blank"><img src="<?php echo TPAP_URL.'/assets/images/coolplugins-logo.png' ?>"/></a>
                        </div>
                        </div>
                
               
            </div>
        </div>
    </form>
        <?php
    }
    function SetAdminStyle() {
        wp_enqueue_style('tpap-editor-styles', TPAP_URL . 'assets/css/tpap-admin-style.css', null, TPAP_VERSION, 'all');
    }
    function action_activate_license(){
        check_admin_referer( 'tpap-license' );
        $licenseKey=!empty($_POST['tpap_license_key'])?$_POST['tpap_license_key']:"";
        $licenseEmail=!empty($_POST['tpap_license_email'])?$_POST['tpap_license_email']:"";
        update_option("TranslatepressAutomaticTranslateAddonPro_lic_Key",$licenseKey) || add_option("TranslatepressAutomaticTranslateAddonPro_lic_Key",$licenseKey);
        update_option("TranslatepressAutomaticTranslateAddonPro_lic_email",$licenseEmail) || add_option("TranslatepressAutomaticTranslateAddonPro_lic_email",$licenseEmail);
        wp_safe_redirect(admin_url( 'admin.php?page='.$this->slug));
    }
    function Activated(){
    ?>
            <form method="post" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>">
                <input type="hidden" name="action" value="TranslatepressAutomaticTranslateAddonPro_el_deactivate_license"/>
                <div class="tpap-license-container">
                    <h3 class="tpap-license-title"><i class="dashicons-before dashicons-translation"></i> <?php _e("Automatic Translate Addon For TranslatePress - Premium License Status",$this->slug);?> </h3>
                    <div class="tpap-license-content">
                        <div class="tpap-license-form">
                            <h3>Active License Status</h3>
                            <ul class="tpap-license-info">
                            <li>
                                <div>
                                    <span class="tpap-license-info-title"><?php _e("License Status",$this->slug);?></span>

                                    <?php if ( $this->responseObj->is_valid ) : ?>
                                        <span class="tpap-license-valid"><?php _e("Valid",$this->slug);?></span>
                                    <?php else : ?>
                                        <span class="tpap-license-valid"><?php _e("Invalid",$this->slug);?></span>
                                    <?php endif; ?>
                                </div>
                            </li>

                            <li>
                                <div>
                                    <span class="tpap-license-info-title"><?php _e("License Type",$this->slug);?></span>
                                    <?php echo $this->responseObj->license_title; ?>
                                </div>
                            </li>

                            <li>
                                <div>
                                    <span class="tpap-license-info-title"><?php _e("License Expiry Date",$this->slug);?></span>
                                    <?php echo $this->responseObj->expire_date; ?>
                                </div>
                            </li>

                            <li>
                                <div>
                                    <span class="tpap-license-info-title"><?php _e("Support Expiry Date",$this->slug);?></span>
                                    <?php echo $this->responseObj->support_end; ?>
                                </div>
                            </li>
                                <li>
                                    <div>
                                        <span class="tpap-license-info-title"><?php _e("Your License Key",$this->slug);?></span>
                                        <span class="tpap-license-key"><?php echo esc_attr( substr($this->responseObj->license_key,0,9)."XXXXXXXX-XXXXXXXX".substr($this->responseObj->license_key,-9) ); ?></span>
                                    </div>
                                </li>
                            </ul>
                            <div class="tpap-license-active-btn">
                                <?php wp_nonce_field( 'tpap-license' ); ?>
                                <?php submit_button('Deactivate License'); ?>
                            </div>
                        </div>
                        <div class="tpap-license-textbox">
                        <h3>Important Points</h3>
                        <ol>
                            <li>1) Please deactivate your license first before moving your website or changing domain.</li>
                            <li>2) Plugin does not auto-translate any string that contains HTML and special characters.</li>
                          
                            <li>3) If you have any issue or query, please <a href="https://coolplugins.net/support/" target="_blank">contact support</a>.</li>
                            <li>4) Automatic translate plugins and themes strings via <a href="https://locoaddon.com/addon/loco-automatic-translate-premium-license-key/" target="_blank">Automatic Translate Addon For Loco Translate</a></li>
                        </ol>
                        <div class="tpap-pluginby">
                            Plugin by<br/>
                            <a href="https://coolplugins.net" target="_blank"><img src="<?php echo TPAP_URL.'/assets/images/coolplugins-logo.png' ?>"/></a>
                        </div>
                        </div>
                    </div>
                </div>
            </form>
    	<?php
    }
    }
new TranslatepressAutomaticTranslateAddonPro();