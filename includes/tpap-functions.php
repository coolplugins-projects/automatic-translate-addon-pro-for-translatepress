<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

        /**
         * Get Data From Database
         * Hooked to wp_ajax_get_strings.
         */
        function tpap_getstrings()
        {
            // Ready for the magic to protect our code
            check_ajax_referer('auto-translate-press-pro-nonces');
            $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
            $pattern = '/\[[^]]+\]/i';
            global $wpdb;
            $result = array();
            $data = array();
            $default_code = isset($_POST['data']) ? sanitize_text_field($_POST['data']) : '';
            $default_language = isset($_POST['default_lang']) ? sanitize_text_field($_POST['default_lang']) : '';
            $current_page_id = isset($_POST['dictionary_id']) ? sanitize_text_field($_POST['dictionary_id']) : '';
            $gettxt_id = isset($_POST['gettxt_id']) ? sanitize_text_field($_POST['gettxt_id']) : '';
            $strings_ID = explode(",", $current_page_id);
            $get_txt_ids = explode(",", $gettxt_id);
            $in_str_arrs = array_fill(0, count($get_txt_ids), '%d');
            $in_strs = join(',', $in_str_arrs);
            $in_str_arr = array_fill(0, count($strings_ID), '%d');
            $in_str = join(',', $in_str_arr);
            $def_lang = strtolower($default_language);
            $table2 = $wpdb->get_blog_prefix() . 'trp_gettext_' . strtolower($default_code);
            $table1 = $wpdb->get_blog_prefix() . 'trp_dictionary_' . $def_lang . '_' . strtolower($default_code);
            $results_gettxt = $wpdb->get_results($wpdb->prepare(
                "SELECT id,original_id,original FROM $table1 WHERE id IN ($in_str) AND  status!='2'", $strings_ID)
            );
            $results = $wpdb->get_results($wpdb->prepare(
                "SELECT id,original FROM $table2 WHERE id IN ($in_strs) AND status!='2'", $get_txt_ids)
            );
            $final_res = array_merge($results_gettxt, $results);
            if (is_array($final_res) && count($final_res) > 0) {
                foreach ($final_res as $row) {
                    $original_id = isset($row->original_id) ? absint($row->original_id) : '';
                    $original = isset($row->original) ? $row->original : '';
                    $string = htmlspecialchars_decode($original);
                    if ($string != strip_tags($string)) {
                        continue;
                    } else if (preg_match($reg_exUrl, $string)) {
                        continue;
                    }
                    else if (preg_match($pattern, $string)) {
                        continue;
                    }
                   
                    if ($original_id == "") {
                        $group = 'Gettext';
                    } else {
                        $group = 'String';
                    }
                    $data['strings'] = $string;
                    $data['database_ids'] = isset($row->id) ? absint($row->id) : '';
                    $data['data_group'] = $group;
                    $result[] = $data;
                }
            }
            echo json_encode($result);
            wp_die();
        }
        /**
         *  save translation from ajax post
         * Hooked to wp_ajax_save_translations.
         */
        function tpap_save_translations()
        {
            // Ready for the magic to protect our code
            check_ajax_referer('auto-translate-press-pro-nonces');
            global $wpdb;
            $strings = filter_var_array(json_decode(stripslashes($_POST['data']), true), FILTER_SANITIZE_STRING);
            if (is_array($strings) && count($strings) > 0) {
                $table1_query = array();
                $table2_query = array();
                $table1 = null;
                $table2 = null;
                foreach ($strings as $languages => $string) {
                    $types = isset($string['data_group']) ? sanitize_text_field($string['data_group']) : '';
                    $default_code = isset($string['language_code']) ? sanitize_text_field($string['language_code']) : '';
                    $default_language = isset($string['default_lang']) ? sanitize_text_field($string['default_lang']) : '';
                    $def_lang = strtolower($default_language);
                    $table2 = $wpdb->get_blog_prefix() . 'trp_gettext_' . strtolower($default_code);
                    $table1 = $wpdb->get_blog_prefix() . 'trp_dictionary_' . $def_lang . '_' . strtolower($default_code);
                    if ($types == "String") {
                        $table_name = sanitize_text_field($table1);
                        $table1_query[] = $string;
                    } else {
                        $table_name = sanitize_text_field($table2);
                        $table2_query[] = $string;
                    }
                }
                if ($table1 != null && $table2 != null) {
                    wp_insert_rows($table1, true, 'id', $table1_query);
                    wp_insert_rows($table2, true, 'id', $table2_query);
                }
                wp_die();
            }
        }
        /**
         *  A method for inserting multiple rows into the specified table
         *  Updated to include the ability to Update existing rows by primary key
         */
        function wp_insert_rows($wp_table_name, $update = false, $primary_key = 'id', $row_arrays = array())
        {
            global $wpdb;
            $wp_table_name = esc_sql($wp_table_name);
            // Setup arrays for Actual Values, and Placeholders
            $values = array();
            $place_holders = array();
            $query = "";
            $query_columns = "";
            $query .= "INSERT INTO `{$wp_table_name}` (";
            foreach ($row_arrays as $count => $row_array) {
                foreach ($row_array as $key => $value) {
                    if (in_array($key, array('data_group', 'original', 'language_code', 'database_id', 'default_lang'))) {
                        continue;
                    }
                    if ($count == 0) {
                        if ($query_columns) {
                            $query_columns .= ", `" . $key . "`";
                        } else {
                            $query_columns .= "`" . $key . "`";
                        }
                    }
                    $values[] = $value;
                    $symbol = "%s";
                    if (is_numeric($value)) {
                        $symbol = "%d";
                    }
                    if (isset($place_holders[$count])) {
                        $place_holders[$count] .= ", '$symbol'";
                    } else {
                        $place_holders[$count] = "( '$symbol'";
                    }
                }
                // mind closing the GAP
                $place_holders[$count] .= ")";
            }
            $query .= " $query_columns ) VALUES ";
            $query .= implode(', ', $place_holders);
            if ($update) {
                $update = " ON DUPLICATE KEY UPDATE `$primary_key`=VALUES( `$primary_key` ),";
                $cnt = 0;
                foreach ($row_arrays[0] as $key => $value) {
                    if (in_array($key, array('data_group', 'original', 'language_code', 'database_id', 'default_lang'))) {
                        continue;
                    }
                    if ($cnt == 0) {
                        $update .= "`$key`=VALUES(`$key`)";
                        $cnt = 1;
                    } else {
                        $update .= ", `$key`=VALUES(`$key`)";
                    }
                }
                $query .= $update;
            }
            $sql = $wpdb->prepare($query, $values);
            if ($wpdb->query($sql)) {
                return true;
            } else {
                return false;
            }
        }


